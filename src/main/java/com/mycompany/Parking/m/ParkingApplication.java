//Clase main
package com.mycompany.Parking.m;

import com.mycompany.Parking.m.logica.Suscripcion;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication //indica el main
@RestController //indica que la clase sera un apirest
public class ParkingApplication {

    //@Autowired // permite la conexion del Jdbc Template
    //JdbcTemplate jdbcTemplate; // instancia de Jdbc Template el cual es quivalente al statrement de la clase conexion
    @Autowired
    Suscripcion sus;

    public static void main(String[] args) {
        SpringApplication.run(ParkingApplication.class, args);
    }

    @GetMapping("/suscripcion")
    public String consultarSuscripcionPorPlaca(@RequestParam(value = "placa", defaultValue = "ABC123") String placa) throws ClassNotFoundException, SQLException {
        sus.setPlaca(placa);
        if (sus.consultar()) {
            return "{\"id_suscripciones\":\"" + sus.getId_suscripciones() + "\",\"placa\":\"" + sus.getPlaca() + "\",\"plaza\":\"" + sus.getPlaza() + "\",\"dueno\":\"" + sus.getDueno() + "\",\"id_dueno\":\"" + sus.getId_dueno() + "\",\"tipo\":\"" + sus.getTipo() + "\",\"fecha_inicio\":\"" + sus.getFecha_inicio() + "\",\"fecha_final\":\"" + sus.getFecha_final() + "\"}";
        } else {
            return "{\"placa\":\"" + placa + "La plaza no tiene asociada una suscripcion." + "\"}";
        }
    }

}
